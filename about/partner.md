# Partner

Eine Bewegung kann sich nicht alleine weiterentwickeln.
Es braucht Partner, die den Weg begleiten.
Wir bedanken uns für die Unterstützung.


## Inhalte

- ![adelphi](assets/partner/adelphi.jpg)
- ![futurzwei](assets/partner/Inhalte_Logo_FuturZwei_300dpi_klein.png)
- ![Diakonie](assets/partner/InhalteUNDFörderer_Diakonie_Wort_Bild_Marke_CMYK.png)
- ![Deutsch Plus](assets/partner/Inhalte_DeutschPlus_LOGO_final_gross_OHNE.png)
- ![60Pages](assets/partner/Inhalte60pages_black.png)
- ![taz meinland](assets/partner/Inhalte_taz-meinland_web.png)
- ![Gesicht Zeigen!](assets/partner/Inhalte_GesichtZwigenLOGO_WEB_72_M.gif)
- ![Intendantengruppe im Deutschen Bühnenverein](assets/partner/Intendantengruppe-im-Deutschen-Buehnenverein.jpg)
- ![Wir machen das](assets/partner/Logo-wirmachendas.png)  

---

## Kreation

- ![Scholz & Friends](assets/partner/ScholzAndFriends.png)
- ![Lucid](assets/partner/lucid.svg)
- ![Scholz & Volkmer](assets/partner/Kreation_scholz-volkmer-gmbhcolorlogo.png )

---

## Aktionen & Events

- ![Deutsches Theater](assets/partner/Aktion_deutsches-theater-logo.png)
- ![Green Music Initative](assets/partner/Aktion_GreenMusicInitiative_Logo-01.svg )
- ![Publixphere](assets/partner/Aktion_Publixphere.png)
- ![Gorki Theater ](assets/partner/Aktion_Gorki.png)
- ![Tiny House University](assets/partner/T Aktion_Tinyhouse_University.png)
- ![youngcaritas](assets/partner/Aktion_ycd_rgb.png)
- ![CAP](assets/partner/Aktion_CAP.png)
- ![Vodafone Stiftung](assets/partner/Förderer_VodafoneStiftungVSD_Logo.png)

---

## Förderer

- ![Diakonie Deutschland](assets/partner/InhalteUNDFörderer_Diakonie_Wort_Bild_Marke_CMYK.png)
- ![Open Society Foundation](assets/partner/Förderer_Open Society Foundations.png)
- ![Robert Bosch Stiftung](assets/partner/FördererBoschStiftungCDToolbox_rbs_logo_rgb.png)
- ![Bertelsmann Stiftung](assets/partner/Förderer_Bertelsmann Stiftung.png)

---

