# Genderise me

1. homepage tile __freund werden__

# Errors

1. open an event/friend from homepage; in overlay click on __kommende aktionen__; then click on browser back button: it is now impossible to close overlay by clicking on X (mac | chrome)
2. __freund werden__, __eigene aktion starten and __mitmachen__ not linked
3. __mit uns in aktion treten, ein leitfaden__ tile should link to http://offenege.kochab.uberspace.de/guideline
4. __fragen und antworten__ tile should link to http://offenege.kochab.uberspace.de/faciliation
5. meta-nav: should say login if i am logged out
6. Aktionen - Termine/Archive: if i click on an event, page reloads (url changes) but i am redirected to the same overview page and no lightbos opens.

# Styling @large
## Homepage

1. all friend-images in grid (.grid .user) with opacity: 0.7; and on hover opacity: 1;
2. image in __About us__ tile is not centered

## Aktionen - termine

1. image missing from events

## Über uns

1. div.team, div.partner and div.contact: should go over the complete width of the site ( only .col-xs-12, no need for .col-md-8.push-md-2 )
2. hr missing above section partner » inhalte
3. hr missing below section partner » förderer

# Styling @small
## Homepage

1. logo image in first tile is not centered
2. more spacing between menu elements: not easily clickable with a normal thumb
3. .big-title font way too small
4. some tiles seem not aligned (could be z-index)
5. image in __About us__ tile is not centered

## Overlays

1. modal dialog´s width cuts out texts ( make width larger and text size smaller ):  
```
.modal .modal-dialog{
	width: 80vw;
}
.event-modal .details{
	.title {
	  font-size: 24px;
	}
	h2{
		font-size: 16px;
	}
	p{
		font-size: 14px ! important;
	}
}
```

## Freunde - Liste

1. text smaller

## Über uns

1. images bigger:  
```
.about .partner ul li img {
  margin: 0.5rem;
  width: calc(100% - 1rem);
}
```

# Impressum

1. Piwik iframe breaks layout (no fixed width)

# SASS for static pages:

```sass
$red: #d20a11;

// general page stylings
h1, .h1 {
  font-size: 2rem;
  font-weight: bold;
  margin: 3rem 0 2rem;
}
.team h1,
.partner h1,
.contact h1{
	color: $red;
}
h2, .h2 {
  font-size: 1.5rem;
  font-weight: bold;
  margin: 2rem 0 1rem;
}
blockquote {
  margin: 0 0 1rem;
  font-size: 1.2rem;
  color: #555;
  padding: 2rem 0 2rem 2rem;
  font-weight: normal;
  strong{ color: $red; float: right; }
}
ul li {
  margin-bottom: 1rem;
}
footer .copyright,
footer .links a {
  color: grey;
  font-size: 0.8rem;
  font-weight: normal;
}

// about page styling
.about{
	.contact ul li,
	.team ul li{
		h2 {
		  margin: 1rem 0;
		}
		p{
		  font-size: 0.8rem;
	    margin-bottom: 0.3rem;
		}
	}
	.partner h2 {
	  font-size: 1.2rem;
	}
}
