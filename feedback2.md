# @large

## Social Sharing

1. Homepage: No image appears when sharing, title is not so nice __Die offene Gesellschaft - Home__
2. Sharing from overlay: meta information refers to page underneath (home or actions or friends)
3. Event page: meta information refers to first event, and not to action page

## Homepage

1. Friend overlay: __Freund werden__ link does not work
2. when opening a link from inside an overlay and then pressing on browser back button, cannot close overlay (X does not work)

## Freund werden

1. cannot register from tablet; no detailed warning is output on what is wrong or missing.